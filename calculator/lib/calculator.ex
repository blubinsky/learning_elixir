defmodule Calculator do

  def parse_input(line) do
    [a, op, b] = String.split(line)
    {a, op, b}
  end

  def compute({a, "+", b}), do: String.to_integer(a) + String.to_integer(b)
  def compute({a, "*", b}), do: String.to_integer(a) * String.to_integer(b)
  def compute({a, "-", b}), do: String.to_integer(a) - String.to_integer(b)
  def compute({a, "/", b}), do: String.to_integer(a) / String.to_integer(b)

  def repl() do
    try do
      IO.gets("Enter a calculation: ")
      |> parse_input()
      |> compute()
    rescue
      MatchError -> IO.puts("Input must be three arguments separated by space.")
      ArgumentError -> IO.puts("Operands must be integers.")
      FunctionClauseError -> IO.puts("I only know +, -, /, *.")
    end
  end
end
